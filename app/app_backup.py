                        
from flask import Flask, request, flash, url_for, redirect, render_template
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///students.sqlite3'
app.config['SECRET_KEY'] = "random string"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

class students(db.Model):
   id = db.Column('student_id', db.Integer, autoincrement=True, primary_key = True)
   name = db.Column(db.String(100))
   city = db.Column(db.String(50))
   addr = db.Column(db.String(200)) 
   pin = db.Column(db.String(10))

def __init__(self, name, city, addr,pin):
   self.name = name
   self.city = city
   self.addr = addr
   self.pin = pin

@app.route('/')
def show_all():
    xquery =  students.query.all()
    return render_template('show_all.html', students=xquery)

@app.route('/register')
def register():
    page_title = {'name':'Book a Desk with QR-code...'}
    if request.method == 'POST':
        if not request.form['lname']  \
            or not request.form['fname'] \
                or not request.form['email']  \
                    or not request.form['pwd1']  \
                        or not request.form['pwd2']:
                        
            flash('Please enter all the fields', 'error')
        else:
            newUser = students(lname=request.form['lname'], \
                fname=request.form['fname'], \
                    email=request.form['email'], \
                        pwd1=request.form['pwd1'], \
                            pwd2=request.form['pwd2'])
            
       
         
   
   
   
   
   
    return render_template('register.html', page_title=page_title)


@app.route('/only')
def only():
    xquery = students.query.filter_by(name='Bernd Mark').first()
    xquery0 = xquery.name
    xquery1 = xquery.city
    print (xquery)
    return render_template('only.html', xquery0=xquery0, xquery1=xquery1)


@app.route('/new', methods = ['GET', 'POST'])
def new():
   if request.method == 'POST':
      if not request.form['name'] or not request.form['city'] or not request.form['addr']:
         flash('Please enter all the fields', 'error')
      else:
         student = students(name=request.form['name'], city=request.form['city'], addr=request.form['addr'], pin=request.form['pin'])
       
         
        # print(student)

         db.session.add(student)
         db.session.commit()
         flash('Record was successfully added')
         return redirect(url_for('show_all'))
   return render_template('new.html')

if __name__ == '__main__':
   db.create_all()
   app.run(debug = True)