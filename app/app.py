#*****************************************************************************
#*****************************************************************************
from crypt import methods
from pickle import TRUE
from flask import Flask, request, flash, url_for, redirect, render_template, session
from flask_login import login_user, logout_user, login_required
from werkzeug.security import generate_password_hash, check_password_hash
from flask_sqlalchemy import SQLAlchemy
import datetime
import uuid
from datetime import datetime, timedelta
import random
import string
#*****************************************************************************
#*****************************************************************************

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///user.sqlite3'
app.secret_key = 'ItShouldBeAnythingButSecret' 
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

class NewUser(db.Model):
   id = db.Column('userid', db.Integer, autoincrement=True, primary_key = True)
   lname = db.Column(db.String(50))
   fname = db.Column(db.String(50))
   email = db.Column(db.String(50)) 
   pwd = db.Column(db.String(100))
   agb = db.Column(db.String(50))
   xcode = db.Column(db.String(50))
   xdate = db.Column(db.String(50))
   xtime = db.Column(db.String(50))
   xregister = db.Column(db.String(2))

def __init__(self, fname, lname, email, pwd, agb, xcode, xdate, xtime, xregister):
   self.fname = lname
   self.lname = fname
   self.email = email
   self.pwd = pwd
   self.agb = agb
   self.xcode = xcode
   self.xdate = xdate
   self.xtime = xtime
   self.xregister = xregister

class Desknr(db.Model):
    idnr = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
    desk = db.Column(db.String(100))
    booking_email = db.Column(db.String(100))
    booking_code = db.Column(db.String(100))

def __init__(self, desk, booking_email, booking_code):
    self.desk = desk
    self.booking_email = booking_email
    self.booking_code = booking_code

class Mydesk(db.Model):
    deskid = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
    userid = db.Column(db.String(100))
    booking_date = db.Column(db.String(100))
    validto = db.Column(db.String(100))
    booked = db.Column(db.String(100)) 
    bookingcode = db.Column(db.String(100)) 
    desk = db.Column(db.String(100))

def __init__(self, userid, booking_date, validto, booked, bookingcode,desk):
    self.userid = userid
    self.booking_date = booking_date
    self.validto = validto
    self.booked = booked
    self.bookingcode = bookingcode
    self.desk = desk

####################### END DATABASE #############################
##################################################################
####################### SHORT POINT ##############################
@app.route('/')
def index():
    page_title = {'name':'Book a Desk...'}
    # cont free desks!
    xcount = Desknr.query.filter_by(booking_email='0', booking_code="0").count()
    return render_template('index.html', page_title=page_title, xcount=xcount)
##################################################################
####################### SHORT QR BOOKING #########################
##################################################################
@app.route('/shortbooking', methods = ['GET', 'POST'])
def shortbooking():
    page_title = {'name':'Book a Desk, short QR-code Booking...'}
    if 'deskme' in request.args:
        deskme = request.args['deskme']
        print(deskme)
        xdesknr = Desknr.query.filter_by(desk=deskme).first()
        if not xdesknr:
            flash('Desk is not valid!')
            return redirect(url_for('shortbooking'))  
    else:
        return redirect(url_for('error'))
  
    if request.method == 'POST':

        email=request.form['email']
        userid_qr = NewUser.query.filter_by(email=email).first()
        xuserid= userid_qr.id

        checkbooking = Mydesk.query.filter_by(userid=xuserid).all()
        if checkbooking:
            flash('your have already booked a desk!, please login and remove first.')
            return redirect(url_for('error'))
    
        xvalid = request.form['validto']    
        user = NewUser.query.filter_by(email=email).first()
        if not user:
            flash('Email address not registerd, you need first add a account.')
            return redirect(url_for('register'))  

        xdesk = Desknr.query.filter_by(booking_email=email).first()
        if xdesk:
            return redirect(url_for('deleteqr'))
    
        if xvalid == "1":
            xxtimestamp = datetime.now()
            xxdate=xxtimestamp.strftime("%d.%m.%Y %H:%M:%S")
            validto = datetime.now() + timedelta(hours=8)
            validto = validto.strftime("%d.%m.%Y %H:%M:%S")
        else:
            xxtimestamp = datetime.now()
            xxdate=xxtimestamp.strftime("%d.%m.%Y %H:%M:%S")
            validto = datetime.now() + timedelta(hours=4) 
            validto = validto.strftime("%d.%m.%Y %H:%M:%S")

        booking_code_ln = 8
        booking_code = ''.join(random.choices(string.ascii_letters+string.digits,k=booking_code_ln))  

        xbook = Mydesk(userid=xuserid,booking_date=xxdate, validto=validto, booked='booked',bookingcode=booking_code,desk=deskme)
        db.session.add(xbook)
        db.session.commit()

        Desknr.query.filter_by(desk=deskme).update(dict(booking_email=email, booking_code=booking_code))
        db.session.commit()

        return redirect(url_for('info'))

    return render_template('shortbooking.html', page_title=page_title, deskme=deskme)
#####################################################################
####################### START REGUSTER ##############################
#####################################################################
@app.route('/register', methods = ['GET', 'POST'])
def register():
    page_title = {'name':'Book a Desk, Registration...'}
    xcount = Desknr.query.filter_by(booking_email='0', booking_code="0").count()
    #date and time
    xtimestamp = datetime.now()
    xdate=xtimestamp.strftime("%d.%m.%Y")
    xtime=xtimestamp.strftime("%H:%M:%S")

    #UUID for registratio-code
    xcode=str(uuid.uuid4())

    #default value registration 0=disabled, 1=vali
    xregister = 0

    if request.method == 'POST':
        if not request.form['lname'] \
            or not request.form['fname']\
                or not request.form['email']\
                    or not request.form['pwd1']\
                        or not request.form['pwd2']\
                            or not request.form['agb']:
            
            flash('Please enter all the fields', 'error')
            return redirect(url_for('register')) 
        else:
            newuser = NewUser(lname=request.form['lname'],\
                fname=request.form['fname'],\
                    email=request.form['email'],\
                            pwd=generate_password_hash(request.form['pwd2'], method='sha256'),\
                                    agb=request.form['agb'],\
                                        xcode=xcode,\
                                            xdate=xdate,\
                                                xtime=xtime,\
                                                    xregister=xregister)

            xpwd1 = request.form['pwd1']
            xpwd2 = request.form['pwd2']
            if xpwd1 != xpwd2:
                flash('Passwords do not match', 'error')
                return redirect(url_for('register')) 

            user = NewUser.query.filter_by(email=request.form['email']).first() # if this returns a user, then the email already exists in database
            if user:
                flash('Email address already exists.')
                return redirect(url_for('register'))   
            else:                     
                db.session.add(newuser)
                db.session.commit() 
                return redirect(url_for('login'))   
    return render_template('register.html', page_title=page_title, xcount=xcount)
#########################################################################
####################### END REGUSTER ####################################
####################### START LOGIN #####################################
#########################################################################
@app.route('/login', methods = ['GET', 'POST'])
def login():

    page_title = {'name':'Book a Desk, Login...'}
    xcount = Desknr.query.filter_by(booking_email='0', booking_code="0").count()
    if request.method == 'POST':
        pwd1=request.form['pwd']
        email=request.form['email']
        
        xlogin = NewUser.query.filter_by(email=email).all()
        
        for row in xlogin:
            id = row.id
            email = row.email
            fname = row.fname
            lname = row.lname
            pwd = row.pwd
            
            verifyPWD=check_password_hash(pwd, pwd1)
            if verifyPWD:
                session['id'] = id
                session['email'] = email
                session['fname'] = fname
                session['lname'] = lname
                session['logged_in'] = True
                return redirect(url_for('dashboard', email=email))
            else:
                print("blöd")
                flash('something goes wrong', 'error')

    return render_template('login.html', page_title=page_title, xcount=xcount)
#########################################################################
####################### END LOGIN #######################################
#########################################################################
####################### START DASHBOARD #################################
#########################################################################
@app.route('/dashboard', methods = ['GET', 'POST'])
def dashboard():

    if 'email' in session:
        username = session['fname']
        id = session['id']
    else:
        flash('something goes wrong', 'please login first')
        return redirect(url_for('login'))

    page_title = {'name':'Book a Desk, Login...'}
    xcount = Desknr.query.filter_by(booking_email='0', booking_code="0").count()
    if 'email' in session:
        username = session['fname']
        email = session['email']
        id = session['id']

        xdesk = Desknr.query.filter_by(booking_email=email).first()
        if xdesk:
            return redirect(url_for('delete'))

        Mydesk.query.filter_by(userid=id).delete()
        xdesk = Desknr.query.filter_by(booking_email='0', booking_code="0").order_by(Desknr.desk.asc()).all()

        if request.method == 'POST':
            desk1 = request.form.get('desk')
            xvalid = request.form.get('validto')

            if xvalid == "1":
                xxtimestamp = datetime.now()
                xxdate=xxtimestamp.strftime("%d.%m.%Y %H:%M:%S")
                validto = datetime.now() + timedelta(hours=8)
                validto = validto.strftime("%d.%m.%Y %H:%M:%S")
            else:
                xxtimestamp = datetime.now()
                xxdate=xxtimestamp.strftime("%d.%m.%Y %H:%M:%S")
                validto = datetime.now() + timedelta(hours=4) 
                validto = validto.strftime("%d.%m.%Y %H:%M:%S")
            
            booking_code_ln = 8
            booking_code = ''.join(random.choices(string.ascii_letters+string.digits,k=booking_code_ln))  
            session['booking_code'] = booking_code

            xbook = Mydesk(userid=id,booking_date=xxdate, validto=validto, booked='booked',bookingcode=booking_code,desk=desk1)
            db.session.add(xbook)
            db.session.commit()

            Desknr.query.filter_by(desk=desk1).update(dict(booking_email=email, booking_code=booking_code))
            db.session.commit()

            return redirect(url_for('booked'))

        return render_template('profile.html', page_title=page_title, username=username,desknr=xdesk, xcount=xcount)
#########################################################################
####################### START INFO ######################################
@app.route('/info')
def info():
    page_title = {'name':'Book a Desk, details...'}
    return render_template('info.html', page_title=page_title)
#########################################################################
####################### END INFO ########################################
####################### SHORT SHOW-ALL ##################################
#########################################################################
@app.route('/showbookings')
def showbookings():
    page_title = {'name':'Book a Desk, short show bookings...'}

    if 'email' in session:
        email = session['email']
        id = session['id']

        print(email)
        print(id)
        
    allbooking = Mydesk.query.all()

    return render_template('allbookins.html', page_title=page_title, allbooking=allbooking)
####################### SHOW USERS ALL ##################################
#########################################################################
@app.route('/showusers')
def showusers():
    page_title = {'name':'Book a Desk, show users...'}
    page_title = {'name':'Book a Desk, details...'}
   
    if 'email' in session:
        username = session['fname']
        id = session['id']
      
        my_users = NewUser.query.all()
    else:
        flash('something goes wrong', 'please login first')
        return redirect(url_for('login'))

    return render_template('showusers.html', page_title=page_title, my_users=my_users)
####################### BOOKING #########################################
#########################################################################
@app.route('/booked')
def booked():
    page_title = {'name':'Book a Desk, details...'}
    if 'email' in session:
        username = session['fname']
        id = session['id']
        
        my_booking = Mydesk.query.filter_by(userid=id).all()
    else:
        flash('something goes wrong', 'please login first')
        return redirect(url_for('login'))

    return render_template('booking.html', page_title=page_title, username=username, my_booking=my_booking)
####################### BOOKING QR DEL ##################################
#########################################################################
@app.route('/deleteqr', methods = ['GET', 'POST'])
def deleteqr():
    page_title = {'name':'Book a Desk, remove QR booking...'}
    if request.method == 'POST':
        qr=request.form['qr']
        
        if not qr:
            flash('please enter a valid booking code')
            return redirect(url_for('deleteqr'))  

        my_booking_code = Desknr.query.filter_by(booking_code=qr).first()

        if not my_booking_code:
            flash('please enter a valid booking code')
            return redirect(url_for('deleteqr'))  
        else:
            Mydesk.query.filter_by(bookingcode=qr).delete()
            Desknr.query.filter_by(booking_code=qr).update(dict(booking_email='0', booking_code="0"))
            db.session.commit()
            flash('booking code was successfully removed!')
            return redirect(url_for('shortbooking')) 
    
    return render_template('removeqr.html', page_title=page_title)
####################### END BOOKING QR DEL ##############################
#########################################################################
####################### DEL USER  #######################################
#########################################################################
@app.route('/deleteuser', methods = ['GET', 'POST'])
def deleteuser():

    if 'register_code' in request.args:
        deleteuser = request.args['register_code']
        print(deleteuser)
        xdeleteuser = NewUser.query.filter_by(xcode=deleteuser).first()

        if xdeleteuser:
            NewUser.query.filter_by(xcode=deleteuser).delete()
            db.session.commit()

        else:
            flash('booking code is not valid!')
            return redirect(url_for('error'))  

    else:
        return redirect(url_for('error'))

    return redirect(url_for('showusers'))
####################### END DEL USER  ###################################
#########################################################################
@app.route('/deleteqr_get', methods = ['GET', 'POST'])
def deleteqr_get():
    page_title = {'name':'Book a Desk, delete QR-code Booking...'}

    if 'booking_code' in request.args:
        booking_code = request.args['booking_code']
        print(booking_code)
        xbookingcode = Desknr.query.filter_by(booking_code=booking_code).first()

        if xbookingcode:
            Mydesk.query.filter_by(bookingcode=booking_code).delete()
            Desknr.query.filter_by(booking_code=booking_code).update(dict(booking_email='0', booking_code="0"))
            db.session.commit()

        else:
            flash('booking code is not valid!')
            return redirect(url_for('error'))  

    else:
        return redirect(url_for('error'))

    return redirect(url_for('showbookings'))


@app.route('/delete')
def delete():
    page_title = {'name':'Book a Desk, Login...'}
    if 'email' in session:
        username = session['fname']
        id = session['id']
        my_booking = Mydesk.query.filter_by(userid=id).all()

    return render_template('error.html', page_title=page_title, username=username, my_booking=my_booking)

@app.route('/remove')
def remove():
    if 'email' in session:
        id = session['id']
        email = session['email']
        
        Desknr.query.filter_by(booking_email=email).update(dict(booking_email='0', booking_code="0"))
        db.session.commit()

        Mydesk.query.filter_by(userid=id).delete()
        db.session.commit()

        return redirect(url_for('dashboard'))

@app.route('/error')
def error():
    page_title = {'name':'Book a Desk, ERROR...'}
    flash('wrong GET-Request')
    return render_template('404.html', page_title=page_title)


@app.route('/logout')
def logout():
    session.clear() 
    return redirect(url_for('login'))

if __name__ == '__main__':
    db.create_all()
    app.run(debug=True, host='0.0.0.0')
